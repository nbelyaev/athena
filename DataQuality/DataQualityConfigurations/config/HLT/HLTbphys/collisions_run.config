# **********************************************************************
# $Id: collisions_run.config 
# **********************************************************************

#######################
# HLTbphys
#######################

#######################
# Output
#######################

output top_level {
  output HLT {
    output TRBPH {
      output Shifter {
        # would better use regexes for chain names here, but it causes crashes - still to be investigated
        output mu11_mu6_bDimu {
          output HLT {
          }
          output OfflineDimu {
          }
        }
        output mu11_mu6_bBmumux_BpmumuKp {
          output HLT {
          }
          output OfflineDimu {
          }
        }
        output mu11_mu6_bBmumux_BdmumuKst {
          output HLT {
          }
          output OfflineDimu {
          }
        }
        output mu11_mu6_bBmumux_BsmumuPhi {
          output HLT {
          }
          output OfflineDimu {
          }
        }
      }
      output Expert {
        output Containers {
          output ${ContainerName} {
          }
        }
        output mu11_mu6_bDimu {
          output HLT {
          }
          output OfflineDimu {
          }
        }
        output mu11_mu6_bBmumux_BpmumuKp {
          output HLT {
          }
          output OfflineDimu {
          }
        }
        output mu11_mu6_bBmumux_BdmumuKst {
          output HLT {
          }
          output OfflineDimu {
          }
        }
        output mu11_mu6_bBmumux_BsmumuPhi {
          output HLT {
          }
          output OfflineDimu {
          }
        }
      }
    }
  }
}


#######################
# References
#######################

#reference Bphys_EBrun_ref {
#  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/
#  file = HLTOfflineReferences_2015_run212967_reproc.root
#  path = run_212967
#  name = same_name
#}


#######################
# Histogram Assessments
#######################

dir HLT {
  
  algorithm = BPhys_HistNotEmpty_YellowEmpty&GatherData

  dir BphysMon {
    
    dir Containers {
      dir (?P<ContainerName>HLT_.*) {
        ## need to define regex = 1 at the first directory level that uses regexes
        ## it is automatically set for all directories/histograms contained inside
        regex = 1
        
        output = HLT/TRBPH/Expert/Containers/${ContainerName}
        display = StatBox
        
        hist ncandidates {
          display = StatBox,LogY
        }
        
        # all the other histograms in the folder
        hist (.*)@Expert {
        }
      }
    }
    
    dir Chains {
      
      algorithm = BPhys_HistKolmogorovTest_MaxDist
      
      # Dimuon chain
      dir HLT_mu11_mu6_bDimu_L1MU8VF_2MU5VF {
        regex = 1
        
        display = StatBox
        output = HLT/TRBPH/Shifter/mu11_mu6_bDimu/HLT
        
        hist ncandidates {
          display = StatBox,LogY,AxisRange(1.5,9.5,"X")
        }
        hist dimu_mass {
        }
        hist dimu_fitmass {
        }
        hist dimu_chi2 {
          display = StatBox,LogY
        }
        hist dimu_pt {
          display = StatBox,LogY
        }
        hist dimu_y {
        }
        hist mu1_pt {
          display = StatBox,LogY
        }
        hist mu2_pt {
          display = StatBox,LogY
        }
        hist mu1_d0 {
          display = StatBox,LogY
        }
        hist mu2_d0 {
          display = StatBox,LogY
        }
        
        # all the others go to Expert
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bDimu/HLT
        }
        
      }
      
      # Bmumux chain
      dir HLT_mu11_mu6_bBmumux_BpmumuKp_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bBmumux_BpmumuKp/HLT
        display = StatBox
        
        hist B_mass {
        }
        hist B_fitmass {
        }
        hist B_chi2 {
          display = StatBox,LogY
        }
        hist B_pt {
          display = StatBox,LogY
        }
        hist B_y {
        }
        
        hist trk_pt {
          display = StatBox,LogY
        }
        hist trk_d0 {
          display = StatBox,LogY
        }
        
        # all the others go to Expert
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bBmumux_BpmumuKp/HLT
        }
        
      }
      
      # Bmumux chain
      dir HLT_mu11_mu6_bBmumux_BdmumuKst_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bBmumux_BdmumuKst/HLT
        display = StatBox
        
        hist B_mass {
        }
        hist B_fitmass {
        }
        hist B_chi2 {
          display = StatBox,LogY
        }
        hist B_pt {
          display = StatBox,LogY
        }
        hist B_y {
        }
        
        hist trk_pt {
          display = StatBox,LogY
        }
        hist trk_d0 {
          display = StatBox,LogY
        }
        
        # all the others go to Expert
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bBmumux_BdmumuKst/HLT
        }
        
      }
      
      # Bmumux chain
      dir HLT_mu11_mu6_bBmumux_BsmumuPhi_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bBmumux_BsmumuPhi/HLT
        display = StatBox
        
        hist B_mass {
        }
        hist B_fitmass {
        }
        hist B_chi2 {
          display = StatBox,LogY
        }
        hist B_pt {
          display = StatBox,LogY
        }
        hist B_y {
        }
        
        hist trk_pt {
          display = StatBox,LogY
        }
        hist trk_d0 {
          display = StatBox,LogY
        }
        
        # all the others go to Expert
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bBmumux_BsmumuPhi/HLT
        }
        
      }
    
    # End of Chains Dir
    }
    
    dir OfflineDimu {
      
      algorithm = BPhys_HistKolmogorovTest_MaxDist
      
      dir HLT_mu11_mu6_bDimu_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bDimu/OfflineDimu
        display = StatBox
        
        hist ncandidates {
          display = StatBox,LogY
        }
        hist dimu_mass {
        }
        hist dimu_chi2 {
          display = StatBox,LogY
        }
        hist dimu_pt {
          display = StatBox,LogY
        }
        hist dimu_y {
        }
        
        # all the rest
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bDimu/OfflineDimu
        }
      }
      
      dir HLT_mu11_mu6_bBmumux_BpmumuKp_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bBmumux_BpmumuKp/OfflineDimu
        display = StatBox
        
        hist ncandidates {
          display = StatBox,LogY
        }
        hist dimu_mass {
        }
        hist dimu_chi2 {
          display = StatBox,LogY
        }
        hist dimu_pt {
          display = StatBox,LogY
        }
        hist dimu_y {
        }
        
        # all the rest
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bBmumux_BpmumuKp/OfflineDimu
        }
      }
      
      dir HLT_mu11_mu6_bBmumux_BdmumuKst_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bBmumux_BdmumuKst/OfflineDimu
        display = StatBox
        
        hist ncandidates {
          display = StatBox,LogY
        }
        hist dimu_mass {
        }
        hist dimu_chi2 {
          display = StatBox,LogY
        }
        hist dimu_pt {
          display = StatBox,LogY
        }
        hist dimu_y {
        }
        
        # all the rest
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bBmumux_BdmumuKst/OfflineDimu
        }
      }
      
      dir HLT_mu11_mu6_bBmumux_BsmumuPhi_L1MU8VF_2MU5VF {
        regex = 1
        
        output = HLT/TRBPH/Shifter/mu11_mu6_bBmumux_BsmumuPhi/OfflineDimu
        display = StatBox
        
        hist ncandidates {
          display = StatBox,LogY
        }
        hist dimu_mass {
        }
        hist dimu_chi2 {
          display = StatBox,LogY
        }
        hist dimu_pt {
          display = StatBox,LogY
        }
        hist dimu_y {
        }
        
        # all the rest
        hist (.*)@Expert {
          output = HLT/TRBPH/Expert/mu11_mu6_bBmumux_BsmumuPhi/OfflineDimu
        }
      }
      
    
    # End of OfflineDimu Dir
    }
    
  # End of BphysMon Dir
  }
  
# End of HLT dir
}

##############
# Algorithms
##############

compositeAlgorithm BPhys_HistNotEmpty_YellowEmpty&GatherData {
  subalgs = Bins_GreaterThan_Threshold,GatherData
  libnames = libdqm_algorithms.so
}

compositeAlgorithm Bphys_YMeanNonZero&BPhys_HistNotEmpty_YellowEmpty&GatherData {
  subalgs = CheckHisto_Mean,Bins_GreaterThan_Threshold,GatherData
  libnames = libdqm_algorithms.so
}

algorithm BPhys_HistNotEmpty_YellowEmpty&GatherData {
  name = BPhys_HistNotEmpty_YellowEmpty&GatherData
  BinThreshold = 0
  thresholds = BPhys_HistNotEmpty_YellowEmpty_Threshold
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_BphysLS:CentrallyManagedReferences_TriggerBphysLS;CentrallyManagedReferences_Trigger
}

algorithm HLTbphys_Histogram_Not_Empty&GatherData {
  libname = libdqm_algorithms.so
  name = HLT_Histogram_Not_Empty&GatherData
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_BphysLS:CentrallyManagedReferences_TriggerBphysLS;CentrallyManagedReferences_Trigger
}

# KolmogorovTest method: scale histograms first
algorithm BPhys_HistKolmogorovTest_MaxDist {
  libname = libdqm_algorithms.so
  name = KolmogorovTest_MaxDist
  thresholds = BPhys_HistKolmogorovTest_MaxDist_Threshold
  MinStat = -1
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_BphysLS:CentrallyManagedReferences_TriggerBphysLS;CentrallyManagedReferences_Trigger
}

# KolmogorovTest method: assume both histograms are scaled
algorithm BPhys_HistKolmogorovTest_Prob {
  libname = libdqm_algorithms.so
  name = KolmogorovTest_Prob
  thresholds = BPhys_HistKolmogorovTest_Prob_Threshold
  MinStat = -1
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_BphysLS:CentrallyManagedReferences_TriggerBphysLS;CentrallyManagedReferences_Trigger
}

# Check containers_size histogram - should have entries with non-zero container size
algorithm Bphys_YMeanNonZero&BPhys_HistNotEmpty_YellowEmpty&GatherData {
  libname = libdqm_algorithms.so
  name = Bphys_YMeanNonZero&BPhys_HistNotEmpty_YellowEmpty&GatherData
  Bins_GreaterThan_Threshold|BinThreshold = 0
  Bins_GreaterThan_Threshold|thresholds = BPhys_HistNotEmpty_YellowEmpty_Threshold
  CheckHisto_Mean|thresholds = Bphys_YMeanNonZero_Threshold
  #MinStat = 10
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_BphysLS:CentrallyManagedReferences_TriggerBphysLS;CentrallyManagedReferences_Trigger
}

###############
# Thresholds
###############

thresholds BPhys_HistNotEmpty_YellowEmpty_Threshold {
  limits NBins {
    error = -0.5
    warning = 0.5
  }
}

thresholds BPhys_HistKolmogorovTest_MaxDist_Threshold {
  limits MaxDist {
    warning = 0.2
    error = 0.5
  }
}

thresholds BPhys_HistKolmogorovTest_Prob_Threshold {
  limits P {
    warning = 0.6
    error = 0.3
  }
}

thresholds Bphys_YMeanNonZero_Threshold {
  limits XMean { # dummy thresholds which never give red flags
    warning = 1000 
    error = 1001
  }
  limits YMean {
    warning = 0.000001
    error = -1
  }
}
