/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#ifndef STRIPSEGMENTTOOL_H
#define STRIPSEGMENTTOOL_H

#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaKernel/IAtRndmGenSvc.h"
#include "CLHEP/Random/RandFlat.h"
#include "CLHEP/Random/RandGauss.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IIncidentListener.h"

#include "MuonDigitContainer/sTgcDigitContainer.h"
#include "MuonDigitContainer/sTgcDigit.h"
#include "MuonIdHelpers/IMuonIdHelperSvc.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonReadoutGeometry/sTgcReadoutElement.h"
#include "MuonRDO/NSW_TrigRawDataContainer.h"
#include "MuonSimData/MuonSimDataCollection.h"
#include "MuonSimData/MuonSimData.h"

#include "RegSelLUT/RegSelSiLUT.h"
#include "RegSelLUT/IRegionIDLUT_Creator.h"
#include "IRegionSelector/IRegSelLUTCondData.h"

#include "TrigT1NSWSimTools/IStripSegmentTool.h"
#include "TrigT1NSWSimTools/PadTrigger.h"
#include "TrigT1NSWSimTools/TriggerTypes.h"
#include "TrigT1NSWSimTools/StripOfflineData.h"
#include "TrigT1NSWSimTools/tdr_compat_enum.h"

#include "TTree.h"
#include <Math/Vector3D.h>
#include <functional>
#include <algorithm>
#include <map>
#include <utility>
#include <cmath>

class IIncidentSvc;
class TTree;

// namespace for the NSW LVL1 related classes
namespace NSWL1 {

  /**
   *
   *   @short interface for the StripTDS tools
   *
   * This class implements the Strip Clustering offline simulation. It loops over the hits,
   * readout from the StripTDSOffLineTool
   *
   *  @author Jacob Searcy <jsearcy@cern.ch>
   *
   *
   */

  class StripSegmentTool: virtual public IStripSegmentTool,
                                  public AthAlgTool,
                                  public IIncidentListener {

  public:
    StripSegmentTool(const std::string& type,
                     const std::string& name,
                     const IInterface* parent);
    virtual ~StripSegmentTool()=default;
    virtual StatusCode initialize() override;
    virtual void handle (const Incident& inc) override;
    virtual StatusCode find_segments( std::vector< std::unique_ptr<StripClusterData> >& ,const std::unique_ptr<Muon::NSW_TrigRawDataContainer>& ) override;

  private:
    ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc {this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};
    SG::ReadCondHandleKey<IRegSelLUTCondData> m_regSelTableKey{this, "RegSelLUT", "RegSelLUTCondData_sTGC", "sTGC Region Selector lookup table"};

    // methods implementing the internal data processing
    StatusCode book_branches();                             //!< book the branches to analyze the StripTds behavior
    void clear_ntuple_variables();                          //!< clear the variables used in the analysis ntuple

    // analysis ntuple
    TTree* m_tree;                                          //!< ntuple for analysis
    // needed Services, Tools and Helpers
    ServiceHandle<IIncidentSvc>  m_incidentSvc     {this, "IncidentSvc", "IncidentSvc"};  //!< Athena/Gaudi incident Service
    Gaudi::Property<std::string> m_sTgcSdoContainer{this, "sTGC_SdoContainerName",  "sTGC_SDO", "Name of the sTGC SDO digit container"};
    Gaudi::Property<bool>        m_doNtuple        {this, "DoNtuple",               false,      "Input StripTds branches into the analysis ntuple"};
    Gaudi::Property<int>         m_rIndexBits      {this, "rIndexBits",              8,         "Number bits in R-index calculation"};
    Gaudi::Property<int>         m_dThetaBits      {this, "dthetaBits",              5,         "Number bits in dTheta calculation"};
    Gaudi::Property<float>       m_dtheta_min      {this, "dthetaMin",             -15.,        "Minimum allowed value for dtheta in mrad"};
    Gaudi::Property<float>       m_dtheta_max      {this, "dthetaMax",              15.,        "Maximum allowed value for dtheta in mrad"};
    Gaudi::Property<int>         m_ridxScheme      {this, "rIndexScheme",            1,         "rIndex slicing scheme/ 0-->R / 1-->eta"};

    // analysis variable to be put into the ntuple
    std::vector<int> *m_seg_wedge1_size;                     //!< theta
    std::vector<int> *m_seg_wedge2_size;                     //!< theta
    std::vector<float> *m_seg_theta;                         //!< theta
    std::vector<float> *m_seg_dtheta;                        //!< delta theta
    std::vector<uint8_t> *m_seg_dtheta_int;
    std::vector<float> *m_seg_eta;                           //!< m_seg_eta
    std::vector<float> *m_seg_eta_inf;
    std::vector<float> *m_seg_phi;
    std::vector<int> *m_seg_bandId;
    std::vector<int> *m_seg_phiId;
    std::vector<int> *m_seg_rIdx;
    std::vector<float> *m_seg_global_x;
    std::vector<float> *m_seg_global_y;
    std::vector<float> *m_seg_global_z;

    StatusCode FetchDetectorEnvelope();
    uint8_t findRIdx(const float&) const;
    uint8_t findDtheta(const float&) const;
    std::pair<float,float> m_zbounds;
    std::pair<float,float> m_etabounds;
    std::pair<float,float> m_rbounds;
  };  // end of StripSegmentTool class
} // namespace NSWL1
#endif
