/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "../TgcRawDataValAlg.h"
#include "../TgcLv1RawDataValAlg.h"
#include "../TgcRawDataMonitorAlgorithm.h"
#include "../TgcRawDataMonitorTool.h"

DECLARE_COMPONENT( TgcRawDataValAlg )
DECLARE_COMPONENT( TgcLv1RawDataValAlg )
DECLARE_COMPONENT( TgcRawDataMonitorAlgorithm )
DECLARE_COMPONENT( TgcRawDataMonitorTool )

